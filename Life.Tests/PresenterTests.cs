﻿using System.IO;
using FluentAssertions;
using Xunit;

namespace Life.Tests
{
	public class PresenterTests
	{
		private readonly Presenter _sut;
		private readonly StringWriter _writer;

		public PresenterTests()
		{
			_writer = new StringWriter();
			_sut = new Presenter(_writer);
		}

		private string Output()
		{
			_writer.Flush();
			_writer.Close();
			return _writer.ToString();
		}
		
		[Fact]
		public void Shows_rows_as_bocks()
		{
			var u = new Universe(2,3);
			_sut.Present(u);

			Output().Should().Be(
				$"{Presenter.Empty}{Presenter.Empty}{Presenter.Empty}\n" +
				$"{Presenter.Empty}{Presenter.Empty}{Presenter.Empty}\n");
		}
	}
}