﻿using System;
using System.Threading;

namespace Life
{
	static class Program
	{
		static void Main()
		{
			var god = new God();
			var presenter = new Presenter(Console.Out);
			var universe = SelectSeederAndConstructUniverse();

			Console.Clear();
			var left = Console.CursorLeft;
			var top = Console.CursorTop;

			while (true)
			{
				Console.CursorLeft = left;
				Console.CursorTop = top;

				presenter.Present(universe);
				presenter.PresentLegend();

				if (!universe.HasAlive)
					break;

				god.Judge(universe);

				Thread.Sleep(1000);
			}
		}

		private static Universe SelectSeederAndConstructUniverse()
		{
			(int row, int col)[] blinker = {(1, 1), (2, 1), (3, 1)};
			(int row, int col)[] glider = {(1, 4), (2, 4), (3, 4), (3, 3), (2, 2)};
			(int row, int col)[] test = {(2, 2), (1, 1), (3, 3), (4, 2)};
			
			var seeder = test;

			Console.WriteLine("Select seed ([1]-blinker, [2]-glider, []-test data):");

			var keyInfo = Console.ReadKey(true);
			
			if ('1' == keyInfo.KeyChar)
				seeder = blinker;
			
			else if ('2' == keyInfo.KeyChar)
				seeder = glider;

			return new Universe(10, 20, seeder);
		}
	}
}