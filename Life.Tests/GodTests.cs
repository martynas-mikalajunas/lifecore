﻿using FluentAssertions;
using Xunit;

namespace Life.Tests
{
	public class GodTests
	{
		private readonly God _sut;

		public GodTests()
		{
			_sut = new God();
		}
		
		[Fact]
		public void Live_cell_with_less_than_2_neighbours_dies()
		{
			var cell = new Cell(0,0, Cell.State.Alive);
			var neighbours = new[] {new Cell(0, 0, Cell.State.Born),};

			_sut.Judge(cell, neighbours).Should().Be(Cell.State.Dead);
		}
		
		[Fact]
		public void Live_cell_with_2_neighbours_survives()
		{
			var cell = new Cell(0,0, Cell.State.Born);
			var neighbours = new[]
			{
				new Cell(0, 0, Cell.State.Born),
				new Cell(0, 0, Cell.State.Alive)
			};

			_sut.Judge(cell, neighbours).Should().Be(Cell.State.Alive);
		}
		
		[Fact]
		public void Live_cell_with_3_neighbours_survives()
		{
			var cell = new Cell(0,0, Cell.State.Born);
			var neighbours = new[]
			{
				new Cell(0, 0, Cell.State.Born),
				new Cell(0, 0, Cell.State.Alive),
				new Cell(0, 0, Cell.State.Alive),
			};

			_sut.Judge(cell, neighbours).Should().Be(Cell.State.Alive);
		}
		
		[Fact]
		public void Live_cell_with_more_than_3_neighbours_dies()
		
		{
			var cell = new Cell(0,0, Cell.State.Alive);
			var neighbours = new[]
			{
				new Cell(0, 0, Cell.State.Born),
				new Cell(0, 0, Cell.State.Alive),
				new Cell(0, 0, Cell.State.Alive),
				new Cell(0, 0, Cell.State.Alive),
			};

			_sut.Judge(cell, neighbours).Should().Be(Cell.State.Dead);
		}
		
		[Fact]
		public void Dead_cell_with_3_neighbours_becomes_alive()
		
		{
			var cell = new Cell(0,0, Cell.State.Dead);
			var neighbours = new[]
			{
				new Cell(0, 0, Cell.State.Born),
				new Cell(0, 0, Cell.State.Alive),
				new Cell(0, 0, Cell.State.Alive),
			};

			_sut.Judge(cell, neighbours).Should().Be(Cell.State.Born);
		}
	}
}