﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Life
{
	public class Universe
	{
		private readonly Cell.State[,] _cells;

		public Universe(int width, int heigth, params (int row, int col)[] seeds)
		{
			_cells = new Cell.State[width, heigth];

			if (null != seeds)
				foreach (var seed in seeds)
					_cells[seed.row, seed.col] = Cell.State.Alive;
		}

		public IEnumerable<Cell> Cells
		{
			get
			{
				for (int i = 0; i <= _cells.GetUpperBound(0); ++i)
				for (int j = 0; j <= _cells.GetUpperBound(1); ++j)
					yield return new Cell(i, j, _cells[i, j]);
			}
		}

		public IEnumerable<Cell> GetNeighbours(Cell cell)
		{
			var rowStart = Math.Max(0, cell.Row - 1);
			var rowEnd = Math.Min(cell.Row + 1, _cells.GetUpperBound(0));

			var colStart = Math.Max(0, cell.Col - 1);
			var colEnd = Math.Min(cell.Col + 1, _cells.GetUpperBound(1));

			for (var i = rowStart; i <= rowEnd; ++i)
			{
				for (var j = colStart; j <= colEnd; ++j)
				{
					if (i == cell.Row && j == cell.Col)
						continue;
					
					yield return new Cell(i, j, _cells[i, j]);
				}
			}
		}

		public void SetCell(Cell cell)
		{
			_cells[cell.Row, cell.Col] = cell.CellState;
		}
		
		public bool HasAlive => Cells.Any(c => c.IsAlive());
	}
}