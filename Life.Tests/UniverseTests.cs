using FluentAssertions;
using Xunit;

namespace Life.Tests
{
	public class UniverseTests
	{
		[Fact]
		public void Can_create_and_seed_universe()
		{
			var sut = new Universe(2, 3, (0, 0), (1, 2));

			foreach (var cell in sut.Cells)
			{
				if (
					(cell.Row == 0 && cell.Col == 0) ||
					(cell.Row == 1 && cell.Col == 2)
				)
				{
					cell.CellState.Should().Be(Cell.State.Alive);
				}
				else
				{
					cell.CellState.Should().Be(Cell.State.Empty);
				}
			}
		}

		[Fact]
		public void Can_get_neighbours_for_cell()
		{
			var sut = new Universe(4, 3);
			sut.GetNeighbours(new Cell(1, 1)).Should().BeEquivalentTo(
				new Cell(0, 0), new Cell(0, 1), new Cell(0, 2), 
				new Cell(1, 0),                 new Cell(1, 2), 
				new Cell(2, 0), new Cell(2, 1), new Cell(2, 2));
		}
		
		[Fact]
		public void Can_get_neighbours_for_top_left_cell()
		{
			var sut = new Universe(4, 3);
			sut.GetNeighbours(new Cell(0, 0)).Should().BeEquivalentTo(
				                 new Cell(0, 1),
				 new Cell(1, 0), new Cell(1, 1));
		}
		
		[Fact]
		public void Can_get_neighbours_for_bottom_right_cell()
		{
			var sut = new Universe(2, 2);
			sut.GetNeighbours(new Cell(1, 1)).Should().BeEquivalentTo(
         new Cell(0, 0), new Cell(0, 1), 
         new Cell(1, 0)                );
		}
	}
}