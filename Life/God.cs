﻿using System.Collections.Generic;
using System.Linq;

namespace Life
{
	public class God
	{
		public void Judge(Universe universe)
		{
			var nextGen = new List<Cell>();

			foreach (var cell in universe.Cells)
			{
				var neighbours = universe.GetNeighbours(cell);
				var newState = Judge(cell, neighbours);
				nextGen.Add(new Cell(cell.Row, cell.Col, newState));
			}

			nextGen.ForEach(universe.SetCell);
		}

		public Cell.State Judge(Cell cell, IEnumerable<Cell> neighbours)
		{
			var aliveNeighbours = neighbours.Count(n => n.IsAlive());

			if (cell.IsAlive())
				return (aliveNeighbours == 2 || aliveNeighbours == 3) ? 
					Cell.State.Alive : Cell.State.Dead;

			return aliveNeighbours == 3 ? 
				Cell.State.Born : Cell.State.Empty;
		}
	}
}