﻿using System;
using System.IO;

namespace Life
{
	public class Presenter
	{
		public const char Empty = '\u2591';
		private const char Dead = '\u2592';
		private const char Born = '\u2593';
		private const char Alive = '\u2588';

		private readonly TextWriter _writer;

		public Presenter(TextWriter writer)
		{
			_writer = writer;
		}

		public void Present(Universe universe)
		{
			int row = 0;
			foreach (var cell in universe.Cells)
			{
				if (cell.Row > row)
				{
					row = cell.Row;
					_writer.Write("\n");
				}

				switch (cell.CellState)
				{
					case Cell.State.Empty:
            _writer.Write(Empty);
						break;
					case Cell.State.Alive:
            _writer.Write(Alive);
						break;
					case Cell.State.Born:
            _writer.Write(Born);
						break;
					case Cell.State.Dead:
            _writer.Write(Dead);
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
      _writer.Write("\n");
		}

		public void PresentLegend()
		{
			var legend = "\n\nLegend:\n" +
			             $"\t{Empty} - Empty cell\n" +
			             $"\t{Born} - Just born cell\n" +
			             $"\t{Alive} - Alive cell\n" +
			             $"\t{Dead} - Dead cell\n";
			
			_writer.WriteLine(legend);
		}
	}
}