﻿namespace Life
{
	public struct Cell
	{
		public enum State
		{
			Empty,
			Alive,
			Born,
			Dead
		}
		
		public Cell(int row, int col, State state = State.Empty)
		{
			Row = row;
			Col = col;
			CellState = state;
		}

		public int Row { get; }
		public int Col { get; }
		public State CellState { get; }

		public bool IsAlive()
		{
			return CellState == State.Alive || CellState == State.Born;
		}
	}
}